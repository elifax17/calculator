package com.kla.calculator.service;

import com.kla.calculator.to.EquationInput;
import com.kla.calculator.to.EquationResult;
import org.apache.log4j.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Arrays;

/**
 * A simple implementation of {@link EquationEvaluationService}.
 */
public class EquationEvaluationServiceImpl implements EquationEvaluationService {

    private static final Logger logger = Logger.getLogger(EquationEvaluationServiceImpl.class);

    /**
     * Actually performs the evaluation of the equation. ScriptEngine is used that treats the
     * operation as a script allowing for functions to be easily handled by passing in a string
     * as opposed to breaking out each function.
     *
     * @param equationInput {@link EquationInput}
     * @return {@link EquationResult}
     */
    public EquationResult evaluateEquation(EquationInput equationInput) {

        final EquationResult equationResult = new EquationResult();

        final ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        final ScriptEngine scriptEngine =  scriptEngineManager.getEngineByName("js");

        try{
            equationResult.setResult(scriptEngine.eval(equationInput.getEquation()).toString());
        } catch(ScriptException e) {
            logger.error("Exception processing equation : " + equationInput.getEquation(), e);

            equationResult.setErredMessages(Arrays.asList("Equation failed to process"));
        }

        return equationResult;
    }
}
