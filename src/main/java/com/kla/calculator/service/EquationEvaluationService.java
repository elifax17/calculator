package com.kla.calculator.service;

import com.kla.calculator.to.EquationInput;
import com.kla.calculator.to.EquationResult;

/**
 * Service for evaluating string equations passed in. Does not perform validation.
 */
public interface EquationEvaluationService {

    /**
     * Evaluates the given string equation passed in via {@link EquationInput}.
     *
     * @param equationInput {@link EquationInput}
     * @return {@link EquationResult} containing the solution to the equation
     */
    EquationResult evaluateEquation(EquationInput equationInput);
}
