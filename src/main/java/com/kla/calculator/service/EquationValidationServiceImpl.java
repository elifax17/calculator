package com.kla.calculator.service;


import com.kla.calculator.to.EquationInput;
import com.kla.calculator.to.EquationResult;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of {@link EquationEvaluationService}.
 */
public class EquationValidationServiceImpl implements EquationValidationService {

    private static final String CALCULATOR_CHARACTERS = "^[\\d\\+\\/\\*\\.\\- \\(\\) ]*$";

    private static final String NON_CALCULATOR_CHARACTERS_ERROR_MESSAGE = "One or more characters entered are not valid, please revise then try again.";

    /**
     * Determines whether any invalid characters have been entered. If so will return with the
     * appropriate error message.
     *
     * @param equationInput {@link EquationInput}
     * @return {@link EquationResult}
     */
    public EquationResult validateEquation(EquationInput equationInput) {

        EquationResult equationResult = new EquationResult();
        List<String> errorList = new ArrayList<>();

        if (!equationInput.getEquation().matches(CALCULATOR_CHARACTERS)) {
            errorList.add(NON_CALCULATOR_CHARACTERS_ERROR_MESSAGE);
        }

        equationResult.setErredMessages(errorList);

        return equationResult;
    }
}
