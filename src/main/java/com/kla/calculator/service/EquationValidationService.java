package com.kla.calculator.service;

import com.kla.calculator.to.EquationInput;
import com.kla.calculator.to.EquationResult;

/**
 * Handles validation for calculator operations, verifying that all functions passed in are valid for processing.
 */
public interface EquationValidationService {

    EquationResult validateEquation(EquationInput equationInput);

}
