package com.kla.calculator.to;

import java.util.List;

/**
 * Handles valid input through the result variable and invalid input through the erredMessages. If none are
 * passed through then there are no errors present.
 */
public class EquationResult {

    private String result;

    private List<String> erredMessages;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<String> getErredMessages() {
        return erredMessages;
    }

    public void setErredMessages(List<String> erredMessages) {
        this.erredMessages = erredMessages;
    }

    @Override
    public String toString() {
        return "EquationResult {" +
                "result='" + result + '\'' +
                ", erredMessage='" + erredMessages + '\'' +
                '}';
    }
}
