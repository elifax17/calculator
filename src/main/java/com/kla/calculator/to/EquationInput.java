package com.kla.calculator.to;

/**
 * Equation submitted in the calculator. Currently only contains equation entered although allows for
 * future enhancements to easily be made.
 */
public class EquationInput {

    private String equation;

    public String getEquation() {
        return equation;
    }

    public void setEquation(String equation) {
        this.equation = equation;
    }

    @Override
    public String toString() {
        return "EquationInput{" +
                "equation='" + equation + '\'' +
                '}';
    }
}
