package com.kla.calculator.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Base controller for the calculator app.
 */
@Controller
public class CalculatorController {

    private static final Logger logger = Logger.getLogger(CalculatorAjaxController.class);

    /**
     * No special information needs to be retrieved when loading the calculator. Also no fooling around
     * with index pages, we just want the calc already!
     * @return {@link ModelAndView}
     */
    @RequestMapping("/")
    public ModelAndView loadCalcuator() {

        logger.debug("Entering load calculator");

        return new ModelAndView("basecalculator");
    }
}
