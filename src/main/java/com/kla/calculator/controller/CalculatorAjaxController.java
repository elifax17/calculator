package com.kla.calculator.controller;

import com.kla.calculator.service.EquationEvaluationService;
import com.kla.calculator.service.EquationValidationService;
import com.kla.calculator.to.EquationInput;
import com.kla.calculator.to.EquationResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller handling ajax requests from the calculator.
 */
@RestController
public class CalculatorAjaxController {

    private static final Logger logger = Logger.getLogger(CalculatorAjaxController.class);

    @Autowired
    EquationValidationService equationValidationService;

    @Autowired
    EquationEvaluationService equationEvaluationService;

    /**
     * Evaluates the equation based upon the values entered in {@link EquationInput} checking them
     * for valid characters.
     *
     * @param equationInput {@link EquationInput}
     * @return {@link EquationResult} containing the result or an error message indicating what went wrong
     */
    @RequestMapping(value = "/evaluate", method = RequestMethod.POST)
    public EquationResult evaluateCalculatorInput(@RequestBody EquationInput equationInput) {

        logger.debug("In evaluateCalculatorInput : " + equationInput);

        EquationResult equationResult = this.equationValidationService.validateEquation(equationInput);

        if (equationResult.getErredMessages().isEmpty()) {
            equationResult = this.equationEvaluationService.evaluateEquation(equationInput);
        }

        logger.debug("Result is : " + equationResult);

        return equationResult;
    }
}
