<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <title>Calculator</title>
    <link href="<c:url value="/resources/css/calculator.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/core/bootstrap.min.css"/>" rel="stylesheet">
    <link rel="icon" href="data:;base64,iVBORw0KGgo=">
    <script src="<c:url value="/resources/core/jquery-1.12.1.min.js" />"></script>
    <script src="<c:url value="/resources/core/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/js/calculator.js" />"></script>
</head>
<body>

<div class="table-responsive">
    <table class="table calculatorTable">
        <tr class="displayRow">
            <td class="calculatorHistory" colspan="5">
                <input id="calculatorHistory" class="input-lg text-right" type="text" disabled="true" value="">
            </td>
        </tr>
        <tr class="displayRow">
            <td class="calculatorDisplay" colspan="5">
                <input id="calculatorDisplay" class="input-lg text-right" type="text" placeholder="insert values here">
            </td>
        </tr>
        <tr>
            <td><a class="btn btn-default btn-block userInputValue">1</a></td>
            <td><a class="btn btn-default btn-block userInputValue">2</a></td>
            <td><a class="btn btn-default btn-block userInputValue">3</a></td>
            <td><a class="btn btn-default btn-block userInputFunction">/</a></td>
            <td></td>
        </tr>
        <tr>
            <td><a class="btn btn-default btn-block userInputValue">4</a></td>
            <td><a class="btn btn-default btn-block userInputValue">5</a></td>
            <td><a class="btn btn-default btn-block userInputValue">6</a></td>
            <td><a class="btn btn-default btn-block userInputFunction">*</a></td>
            <td><a class="btn btn-default btn-block userInputBackspace"><--</a></td>
        </tr>
        <tr>
            <td><a class="btn btn-default btn-block userInputValue">7</a></td>
            <td><a class="btn btn-default btn-block userInputValue">8</a></td>
            <td><a class="btn btn-default btn-block userInputValue">9</a></td>
            <td><a class="btn btn-default btn-block userInputFunction">-</a></td>
            <td><a class="btn btn-default btn-block userInputClear">C</a></td>
        </tr>
        <tr>
            <td colspan="2"><a class="btn btn-default btn-block userInputValue">0</a></td>
            <td><a class="btn btn-default btn-block userInputValue">.</a></td>
            <td><a class="btn btn-default btn-block userInputFunction">+</a></td>
            <td><a class="btn btn-default btn-block userInputSubmit">=</a></td>
        </tr>
    </table>
</div>
</body>
</html>
