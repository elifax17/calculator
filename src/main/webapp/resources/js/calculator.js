$(document).ready(function () {
    calculator.addOnClickToUserInput();
});


var calculator = {

    formSubmitted: false,

    addOnClickToUserInput: function () {
        var $calculatorDisplay = $("#calculatorDisplay");
        var $calculatorHistory = $("#calculatorHistory");

        $(".userInputValue").on("click", function () {
            $calculatorDisplay.val($calculatorDisplay.val() + $(this).html());
        });

        $(".userInputFunction").on("click", function () {
            $calculatorDisplay.val($calculatorDisplay.val() + $(this).html());
        });

        $(".userInputBackspace").on("click", function () {
            $calculatorDisplay.val(
                function (index, value) {
                    return value.substr(0, value.length - 1);
                });
        });

        $(".userInputClear").on("click", function () {
            calculator.removeErrorDisplay();
            $calculatorDisplay.val("");
            $calculatorHistory.val("");
        });

        $(".userInputSubmit").on("click", function () {
            $calculatorHistory.val("").val($calculatorDisplay.val());
            calculator.submitEquation();
        });
    },

    submitEquation: function () {

        calculator.removeErrorDisplay();

        //Passing in a json object for future usage
        var equationJSON = {"equation": $("#calculatorDisplay").val()};

        if (!calculator.formSubmitted) {
            calculator.handleFormSubmit(true);

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "evaluate",
                data: JSON.stringify(equationJSON),
                timeout: 10000,
                success: function (data) {
                    calculator.handleFormSubmit(false);
                    calculator.handleEquationResults(data);

                },
                error: function (erredData) {
                    calculator.handleFormSubmit(false);
                    calculator.handleEquationResults(erredData)
                }
            });
        }
    },

    handleEquationResults: function (data) {
        if (data.erredMessages != null && data.erredMessages.length > 0) {
            calculator.addErrorToDisplay(data.erredMessages);
        } else {
            $("#calculatorDisplay").val(data.result);
        }
    },

    addErrorToDisplay: function (errorMessages) {

        var errorTooltipMessage = "";

        $.each(errorMessages, function (i, item) {
            if (i != 0) {
                errorTooltipMessage += "\n";
            }
            errorTooltipMessage += item;
        });

        $("#calculatorDisplay").addClass("error").attr("title", errorTooltipMessage).tooltip();
    },

    removeErrorDisplay: function () {
        $("#calculatorDisplay").removeClass("error").attr("title", "").tooltip("destroy");
    },

    handleFormSubmit : function(isFormSubmitted) {
        if (isFormSubmitted) {
            calculator.formSubmitted = true;
            //enable processing indicator
        } else {
            calculator.formSubmitted = false;
            //disable processing indicator
        }

    }
}