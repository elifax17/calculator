# README #

### Shawn's Calculator App ###


### How do I get set up? ###

With INTELLIJ

	https://bitbucket.org/elifax17/calculator/downloads (I did this over using source control as Intellij seems to not play as nicely with bitbucket or maybe I just need the plugin)

	New project from existing sources
	Import project from external model - Maven
	Set up tomcat server
	- Download from https://tomcat.apache.org/download-70.cgi
	- Add new from run/debug configurations just need to click configuration and set path (Default options should be fine)
	- Add calculator exploded to deployment (.idea files may handle this)
	Start server, should go to localhost:8080

WITHOUT INTELLIJ

	Install maven - https://maven.apache.org/install.html

	Download Apache following whatever local config you have - https://tomcat.apache.org/download-70.cgi
	- Make sure JAVA_HOME is set with a jdk

	Download calculator repository from https://bitbucket.org/elifax17/calculator/downloads

	At this point you can either manually drop the war into the webapps folder of apache or use maven plugin to deploy it

	MANUAL DEPLOY

		Navigate your command prompt to the calculator folder and run "mvn clean install"
		Look in the target folder and copy the calculator.war then paste that into tomcat's webapps folder
		Start the server and go to localhost:8080/calculator

	MAVEN DEPLOY

		Place extracted calculator folder in same root as apache 
		OR
		modify the pom.xml to point to whatever location apache webapps folder is <deployFolder>../apache-tomcat-7.0.68/webapps</deployFolder>

		Go to tomcat-users.xml in /apache-tomcat-7.0.68/conf and put (you can always modify the pom to your liking as well)

		<role rolename="manager-script"/>
		<user username="admin" password="admin" roles="manager-script"/>

		Start your server

		Open command prompt in the calculator folder then do "mvn clean install tomcat7:deploy"

		Go to localhost:8080/calculator